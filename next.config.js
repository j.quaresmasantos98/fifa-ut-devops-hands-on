/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains: ["media.contentapi.ea.com"],
  },
};

module.exports = nextConfig;
