import { CARD_URL, PORTRAIT_URL, URL } from '../constants'
import { fetchHttp } from '../infra/http'
import { Player } from '../types/player'

export async function playersService (): Promise<Player[]> {
  const { docs }: { docs: any[] } = await fetchHttp(URL)

  const players: Player[] = docs.map(({ primaryKey, playerjerseyname }) => ({
    id: primaryKey,
    name: playerjerseyname,
    portraitUrl: `${PORTRAIT_URL}/${primaryKey}.png`,
    cardUrl: `${CARD_URL}/${primaryKey}.png`
  }))

  return players
}
