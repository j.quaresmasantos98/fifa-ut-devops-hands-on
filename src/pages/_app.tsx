import LogRocket from 'logrocket'
import '../styles/globals.css'
import type { AppProps } from 'next/app'
import '../components/players/index.css'
import '../components/player-selected/index.css'

LogRocket.init('1pztna/fifa-ut')

function MyApp ({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}

export default MyApp
