export interface Player {
  id: number
  name: string
  portraitUrl: string
  cardUrl: string
}
