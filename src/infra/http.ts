export const fetchHttp = async (url: string): Promise<any> => {
  return await (await fetch(url)).json()
}
