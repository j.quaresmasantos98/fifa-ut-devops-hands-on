import LogRocket from 'logrocket'
import Image from 'next/image'
import { useState } from 'react'
import { Player } from '../../types/player'
import PlayerSelected from '../player-selected'

interface Props {
  players: Player[]
}

const Players = ({ players }: Props) => {
  const [playerSelectedIndex, setPlayerSelectedIndex] = useState<number>(-1)

  const onClickCardPlayer = (index: number, { id, name }: Player) => {
    setPlayerSelectedIndex(index)

    LogRocket.track('Selected Player', { id, name })
  }

  return (
    <div className="container">
      <div className="players">
        {players.map((player, index) => (
          <div
            key={player.id}
            className={`player-portrait ${
              index === playerSelectedIndex ? 'selected' : ''
            }`}
            onClick={() => onClickCardPlayer(index, player)}
          >
            <Image
              src={player.portraitUrl}
              alt={player.name}
              width={180}
              height={180}
            />
            <hr />
            <p>{player.name}</p>
          </div>
        ))}
      </div>
      <PlayerSelected player={players[playerSelectedIndex]} />
    </div>
  )
}

export default Players
