import Image from 'next/image'
import { Player } from '../../types/player'

interface Props {
  player: Player | null
}

const PlayerSelected = ({ player }: Props) => {
  return (
    <div className="player-selected">
      {(player != null)
        ? (
        <Image
          src={player.cardUrl}
          alt={player.name}
          width={524 / 2}
          height={828 / 2}
        />
          )
        : (
        <p className="empty-player-selected">Select some Player</p>
          )}
    </div>
  )
}

export default PlayerSelected
