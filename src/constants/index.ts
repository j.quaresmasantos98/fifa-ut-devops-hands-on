export const BASE_URL = 'https://ratings-api.ea.com/v2'
export const URL = `${BASE_URL}/entities/fifa-23-ratings?filter=&sort=ranking%3AASC&limit=40&offset=0`

export const MEDIA_URL =
  'https://media.contentapi.ea.com/content/dam/ea/fifa/fifa-23/ratings/common'

export const PORTRAIT_URL = `${MEDIA_URL}/player-small-portraits`
export const CARD_URL = `${MEDIA_URL}/player-headshots`
