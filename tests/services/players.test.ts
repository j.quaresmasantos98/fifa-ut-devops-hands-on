import Sinon from "sinon";
import { CARD_URL, PORTRAIT_URL } from "../../src/constants";
import { playersService } from "../../src/services/players";

jest.mock("../../src/infra/http", () => ({
  fetchHttp: async () => {
    return {
      docs: [{ primaryKey: 1, playerjerseyname: "23" }],
    };
  },
}));

describe("players", () => {
  beforeEach(Sinon.restore);

  it("fetch all", async () => {
    const result = await playersService();

    expect(result).toStrictEqual([
      {
        id: 1,
        name: "23",
        cardUrl: `${CARD_URL}/1.png`,
        portraitUrl: `${PORTRAIT_URL}/1.png`,
      },
    ]);
  });
});
