import PlayerSelected from "../../../src/components/player-selected";
import Players from "../../../src/components/players";
import { render } from "../../test-utils";

describe("PlayerSelected", () => {
  it("renders player selected", () => {
    const { getByAltText } = render(
      <PlayerSelected
        player={{
          id: 1,
          name: "Messi",
          cardUrl: "http://cardUrl",
          portraitUrl: "http://portraitUrl",
        }}
      />
    );

    expect(getByAltText("Messi")).toBeInTheDocument();
  });

  it("does not render player selected", () => {
    const { getByText } = render(<PlayerSelected player={null} />);

    expect(getByText("Select some Player")).toBeInTheDocument();
  });
});
