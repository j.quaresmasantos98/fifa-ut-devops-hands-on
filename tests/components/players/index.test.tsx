import Players from "../../../src/components/players";
import { render } from "../../test-utils";

describe("Players", () => {
  it("renders players", () => {
    const { getByText } = render(
      <Players
        players={[
          {
            id: 1,
            name: "Messi",
            cardUrl: "http://cardUrl",
            portraitUrl: "http://portraitUrl",
          },
          {
            id: 2,
            name: "Neymar",
            cardUrl: "http://cardUrl",
            portraitUrl: "http://portraitUrl",
          },
        ]}
      />
    );

    expect(getByText("Messi")).toBeInTheDocument();
    expect(getByText("Neymar")).toBeInTheDocument();
  });
});
